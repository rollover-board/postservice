package posts

import (
	"context"
	"errors"
	"log"
	"sync"
	"time"

	"github.com/rs/xid"
	"gitlab.com/rollover-board/postservice/filter/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	ErrNotFound     = errors.New("not found")
	ErrFailedFilter = errors.New("disallowed post")
)

func newConnection(serverAddr string) proto.ValidateServiceClient {

	log.Print("Connecting to filter service at " + serverAddr)
	conn, err := grpc.Dial(serverAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("fail to dial: %v", err)
	} else {
		log.Print("Successfully connected to " + serverAddr + " conn is : " + conn.GetState().String())
	}

	client := proto.NewValidateServiceClient(conn)

	return client
}

type Config struct {
	Listen map[string]string
	Filter map[string]string
}

type Post struct {
	ID      string    `json:"id"`
	Title   string    `json:"title"`
	Created time.Time `json:"created"`
	Content string    `json:"content"`
}

type PostService interface {
	CreatePost(ctx context.Context, Title, Content string) (string, error)
	ReadPostByID(ctx context.Context, id string) (Post, error)
	UpdatePost(ctx context.Context, post Post) error
	DeletePost(ctx context.Context, id string) error
	ReadAllPosts(ctx context.Context) ([]Post, error)
}

type PostServiceImpl struct {
	sync.RWMutex
	posts map[string]Post
	conn  proto.ValidateServiceClient
	cfg   *Config
}

func NewPostService(cfg *Config) PostService {
	ret := new(PostServiceImpl)
	ret.posts = map[string]Post{}
	filterUrl := cfg.Filter["url"]

	log.Println("Filter url ", filterUrl)
	if len(filterUrl) == 0 {
		filterUrl = "127.0.0.1:6000"
	}

	ret.conn = newConnection(filterUrl)
	ret.cfg = cfg

	return ret
}

func (svc *PostServiceImpl) InsertPost(title, content string) (string, error) {

	post := Post{}
	post.Title = title
	post.Content = content
	post.ID = xid.New().String()
	post.Created = time.Now()
	svc.posts[post.ID] = post

	log.Println("Created post with id " + post.ID)
	return post.ID, nil

}

func (svc *PostServiceImpl) CreatePost(ctx context.Context, title, content string) (string, error) {
	svc.Lock()
	defer svc.Unlock()

	req := proto.ValidateRequest{Title: title, Content: content}
	resp, err := svc.conn.ValidatePost(ctx, &req)

	if err != nil {
		log.Println("Failed to filter post")
		log.Println(err.Error())
		return svc.InsertPost(title, content)
	} else if resp.Action == proto.ValidationAction_PROCEED {
		log.Println("Post filtered ok")
		return svc.InsertPost(title, content)
	} else if resp.Action == proto.ValidationAction_DISCARD {
		log.Println("Post filtered discard")
		return "", ErrFailedFilter
	}

	return "", nil
}

func (svc *PostServiceImpl) ReadPostByID(ctx context.Context, id string) (Post, error) {
	log.Println("ReadPostById ", id)
	svc.Lock()
	defer svc.Unlock()

	post, ok := svc.posts[id]

	if ok {
		log.Println("found post "+post.ID, post.Title)
		return post, nil
	}
	log.Println("Failed to find post")
	return Post{}, ErrNotFound
}

func (svc *PostServiceImpl) UpdatePost(ctx context.Context, post Post) error {
	log.Println("UpdatePost")
	svc.Lock()
	defer svc.Unlock()
	svc.posts[post.ID] = post
	return nil
}

func (svc *PostServiceImpl) DeletePost(ctx context.Context, id string) error {
	svc.Lock()
	defer svc.Unlock()

	log.Println("deleting ", id)
	delete(svc.posts, id)

	return nil
}

func (svc *PostServiceImpl) ReadAllPosts(ctx context.Context) ([]Post, error) {
	svc.Lock()
	defer svc.Unlock()

	plen := len(svc.posts)
	log.Println("Have ", plen, " posts")
	posts := make([]Post, 0, plen)

	for _, val := range svc.posts {
		posts = append(posts, val)
	}

	return posts, nil
}
