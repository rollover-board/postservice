package posts

import (
	"context"
	"net/http"
	"testing"
)

func TestDecodeCreateRequest(t *testing.T) {

	ctx := context.Background()
	req, _ := http.NewRequest("GET", "127.0.0.1/1", nil)
	req.Header.Set("id", "1")

	res, _ := decodeReadRequest(ctx, req)

	reqCast, _ := res.(ReadRequest)

	if reqCast.ID != "1" {
		t.Errorf("got %q, wanted %q", reqCast.ID, "1")
	}

}
