package posts

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	httptransport "github.com/go-kit/kit/transport/http"
)

var ErrMissingParam = errors.New("Missing parameter")

func NewHTTPHandler(pe PostsEndpoints) http.Handler {

	router := chi.NewRouter()

	router.Get("/{id}", httptransport.NewServer(
		pe.ReadPostByIDEndpoint,
		decodeReadRequest,
		encodeResponse,
	).ServeHTTP)

	router.Post("/", httptransport.NewServer(
		pe.CreatePostEndpoint,
		decodeCreateRequest,
		encodeResponse,
	).ServeHTTP)

	router.Get("/", httptransport.NewServer(
		pe.ReadAllPostsEndpoint,
		decodeGetAllRequest,
		encodeResponse,
	).ServeHTTP)

	router.Delete("/{id}", httptransport.NewServer(
		pe.DeletePostEndpoint,
		decodeDeleteRequest,
		encodeResponse,
	).ServeHTTP)

	router.Mount("/posts", router)
	return router
}

func decodeReadRequest(_ context.Context, r *http.Request) (interface{}, error) {
	fmt.Println("decoding")

	id := chi.URLParam(r, "id")
	if id == "" {
		fmt.Println("err decoding")
		return nil, ErrMissingParam
	}

	fmt.Println("decoded ok")
	return ReadRequest{ID: id}, nil
}

func decodeDeleteRequest(_ context.Context, r *http.Request) (interface{}, error) {
	fmt.Println("decoding delete")

	id := chi.URLParam(r, "id")
	if id == "" {
		fmt.Println("err decoding delete")
		return nil, ErrMissingParam
	}

	fmt.Println("decoded ok")
	return DeleteRequest{ID: id}, nil
}

func decodeGetAllRequest(_ context.Context, r *http.Request) (interface{}, error) {
	req := ReadAllRequest{}
	return req, nil
}

func decodeCreateRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req CreateRequest

	err := json.NewDecoder(r.Body).Decode(&req)

	if err != nil {
		return nil, err
	}

	return req, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	err, ok := response.(error)

	fmt.Println("encoding response")

	if ok && err != nil {
		fmt.Println("error encoding response")
		encodeError(ctx, err, w)
		return nil
	}

	encErr := json.NewEncoder(w).Encode(response)
	if encErr != nil {
		fmt.Println("Response encode Failure")
	}

	return encErr
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}
