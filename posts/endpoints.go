package posts

import (
	"context"

	"github.com/go-kit/kit/endpoint"
)

type PostsEndpoints struct {
	CreatePostEndpoint   endpoint.Endpoint
	ReadPostByIDEndpoint endpoint.Endpoint
	UpdatePostEndpoint   endpoint.Endpoint
	DeletePostEndpoint   endpoint.Endpoint
	ReadAllPostsEndpoint endpoint.Endpoint
}

func MakePostsEndpoints(svc PostService) PostsEndpoints {
	return PostsEndpoints{
		CreatePostEndpoint:   MakeCreateEndpoint(svc),
		ReadPostByIDEndpoint: MakeReadEndpoint(svc),
		UpdatePostEndpoint:   MakeUpdateEndpoint(svc),
		DeletePostEndpoint:   MakeDeleteEndpoint(svc),
		ReadAllPostsEndpoint: MakeReadAllEndpoint(svc),
	}
}

// Create
type CreateRequest struct {
	Title   string
	Content string
}

type CreateResponse struct {
	ID string
}

func MakeCreateEndpoint(svc PostService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateRequest)
		id, err := svc.CreatePost(ctx, req.Title, req.Content)
		return CreateResponse{ID: id}, err
	}
}

// Read
type ReadRequest struct {
	ID string
}

type ReadResponse struct {
	Post Post `json:"post"`
}

func MakeReadEndpoint(svc PostService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(ReadRequest)
		post, err := svc.ReadPostByID(ctx, req.ID)
		return ReadResponse{Post: post}, err
	}
}

// Update
type UpdateRequest struct {
	Post Post `json:"post"`
}

type UpdateResponse struct {
	Post Post `json:"post"`
}

func MakeUpdateEndpoint(svc PostService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(UpdateRequest)
		err := svc.UpdatePost(ctx, req.Post)
		return UpdateResponse{req.Post}, err
	}
}

// Delete
type DeleteRequest struct {
	ID string `json:"ID"`
}

type DeleteResponse struct {
}

func MakeDeleteEndpoint(svc PostService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteRequest)
		svc.DeletePost(ctx, req.ID)
		return DeleteResponse{}, nil
	}
}

// ReadAll

type ReadAllRequest struct {
}

type ReadAllResponse struct {
	Posts []Post `json:"posts"`
}

func MakeReadAllEndpoint(svc PostService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		posts, err := svc.ReadAllPosts(ctx)
		ret := ReadAllResponse{posts}
		return ret, err
	}
}
