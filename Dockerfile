FROM golang:1.17.8-alpine3.14
ENTRYPOINT ["/bin/postservice","-config","/conf/postservice.toml"]
EXPOSE 8000
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
COPY ./conf/postservice-k8s.toml /conf/postservice.toml
COPY ./postservice /bin/postservice
