package main

import (
	"flag"
	"log"
	"net/http"
	"strings"

	"github.com/BurntSushi/toml"

	"gitlab.com/rollover-board/postservice/posts"
)

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}

func main() {

	var cfgFileName string
	flag.StringVar(&cfgFileName, "config", "conf/postservice.toml", "properties file with service config")
	flag.Parse()
	log.Print("using config file " + cfgFileName)

	var config = posts.Config{}
	config.Filter = make(map[string]string)
	config.Listen = make(map[string]string)

	toml.DecodeFile(cfgFileName, &config)

	svc := posts.NewPostService(&config)
	endpoints := posts.MakePostsEndpoints(svc)
	handler := posts.NewHTTPHandler(endpoints)
	handler = accessControl(handler)

	//listenAddr := props.GetString("listen.address", ":8000")
	listenAddr := config.Listen["address"]
	log.Println("listen address ", listenAddr)
	listenAddr = strings.TrimSpace(listenAddr)
	log.Print("Listening on " + listenAddr)
	err := http.ListenAndServe(listenAddr, handler)
	if err != nil {
		panic(err)
	}
}
